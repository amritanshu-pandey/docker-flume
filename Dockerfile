FROM centos:7
RUN yum update -y && \
    yum install wget -y
RUN cd /tmp && wget --no-check-certificate --no-cookies \
    --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    "https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.tar.gz"

RUN tar xzvf /tmp/jdk-8u201-linux-x64.tar.gz -C /opt/
ENV JAVA_HOME=/opt/jdk1.8.0_201
ENV PATH=$JAVA_HOME/bin:$PATH

RUN cd /tmp && wget "http://mirror.ventraip.net.au/apache/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz"
RUN tar xzvf /tmp/apache-flume-1.9.0-bin.tar.gz -C /opt/

ENV FLUME_HOME=/opt/apache-flume-1.9.0-bin
ENV PATH=$FLUME_HOME/bin:$PATH

CMD java -version && flume-ng version
