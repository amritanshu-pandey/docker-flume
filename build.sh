#!/bin/bash
set -eu

VERSION=0.1.0
IMAGE_NAME=amritanshu16/docker-flume

docker build -t ${IMAGE_NAME}:${VERSION} .
