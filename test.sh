#!/bin/bash
set -eu

echo "Run Flume process"
docker run -v `pwd`/fixtures/:/config amritanshu16/docker-flume:0.1.0 /bin/bash /config/docker-test.sh

echo "Test output"
diff ./fixtures/output.txt ./fixtures/input_data.txt
