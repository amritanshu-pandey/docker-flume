#!/bin/bash
set -eu

echo "Create required directories"
mkdir /tmp/{spool,output}

echo "Start flume agent"
flume-ng agent --conf $FLUME_HOME/conf --conf-file /config/flume.conf --name a1 -Dflume.root.logger=INFO,console &

echo "Sleep for 5 seconds"
sleep 5

echo "Copy input data to spoolDir"
cp /config/input_data.txt /tmp/spool

echo "Sleep for 5 seconds"
sleep 5

echo "Copy the output file to Docker host dir"
cp /tmp/output/*.out /config/output.txt
